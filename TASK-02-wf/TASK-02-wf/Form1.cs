﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TASK_02_wf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            label1.Text = q1;

        }
        static string q1 = "Please enter the price of the item: ";
        static double totalcost = 0;
        static double itemprice = 0;

        static string gst(double totalcost)
        {

            var output = "";
            totalcost = totalcost * 1.15;
            totalcost = System.Math.Round(totalcost, 2);
            output = Convert.ToString(totalcost);

            return output;

        }

      
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            itemprice = double.Parse(textBox1.Text);
            totalcost = itemprice + totalcost;

            label2.Text = $"Added amount ${itemprice}";
            textBox1.Text = String.Empty;
        }

        private void button2_Click_1(object sender, EventArgs e)
        {

            var answer = "";
            answer = $"The total cost of the shopping including GST is \n${gst(totalcost)}";

            label3.Text = answer;
            textBox1.Text = String.Empty;
        }
    }
}
  












