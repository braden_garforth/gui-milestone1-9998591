﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK_02_console
{
    class Program
    {
        static int cloops = 0;

        static void Main(string[] args)
        {
            while (true)
            {
                if (cloops < 1)
                {
                    Console.WriteLine("ADD GST!\n");
                    cloops++;
                }

                Console.WriteLine(q1);
                itemprice = Convert.ToDouble(Console.ReadLine());

                Console.WriteLine($"Added amount ${itemprice}\n");
                Console.WriteLine(q2);
                var option = int.Parse(Console.ReadLine());

                Console.WriteLine($"{loop1(itemprice, option)}");


                if (option == 1)
                {
                    Console.WriteLine(q3);
                    Console.ReadLine();
                    break;
                }
            }
        }


        static string q2 = "Would you like to add another item to the shopping list? Type [0]  to continue, type [1] to finish the list. ";
        static string q3 = "Press the \"Enter\" key to exit.";
        static string q1 = "Please enter the price of the item: ";
        static double totalcost = 0;
        static double itemprice = 0;


        static string loop1(double itemprice, int option)
        {
            var answer = "";

            switch (option)
            {
                case 0:

                    totalcost = itemprice + totalcost;
                    break;

                case 1:

                    totalcost = itemprice + totalcost;
                    answer = $"\nThe total cost of the shopping list including GST is ${gst(totalcost)}";
                    break;

                default:

                    answer = $"Type \"0\"  to continue or type \"1\" to finish the list.";
                    break;
            }

            return answer;
        }


        static string gst(double totalcost)
        {

            var output = "";

            totalcost = totalcost * 1.15;
            output = Convert.ToString(totalcost);

            return output;

        }
    }
}
