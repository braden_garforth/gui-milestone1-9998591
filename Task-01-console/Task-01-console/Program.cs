﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_01_console
{
    class Program
    {
        static string q1 = "Enter the amount of distance you want to convert";
        static string q2 = "Do you want to convert it to [0]Miles or [1]Kilometers?";

        static void Main(string[] args)
        {
            Console.WriteLine(q1);
            var number = Console.ReadLine();

            Console.WriteLine(q2);
            var choice1 = int.Parse(Console.ReadLine());

            Console.WriteLine($"{choices(number, choice1)}");

            Console.WriteLine($"To exit, press any key.");
            Console.ReadLine();

        }


        static string choices(string number, int choice)
        {
            var answer = "";

            switch (choice)
            {
                case 0:
                    answer = $"{number} Kilometers is equal to {convertmile(double.Parse(number))} Miles";
                    break;
                case 1:
                    answer = $"{number} Miles is equal to {convertkm(double.Parse(number))} Kilometers";
                    break;
                default:
                    answer = $"Incorrect value given, use 0 for Kilometers and 1 for Miles";
                    break;
            }

            return answer;
        }

        static double convertmile(double input)
        {
            const double converter = 1.609344;
            var mile = Convert.ToDouble(input);
            var result = mile * converter;

            result = System.Math.Round(result, 2);
            return result;
        }

        static double convertkm(double input)
        {
            const double converter = 0.621371192;
            var km = Convert.ToDouble(input);
            var result = km * converter;

            result = System.Math.Round(result, 2);
            return result;
        }

    }
}