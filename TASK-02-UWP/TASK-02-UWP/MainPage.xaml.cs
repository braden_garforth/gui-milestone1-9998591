﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace TASK_02_UWP
{

    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            label1.Text = q1;
        }

        private void bt1_Click(object sender, RoutedEventArgs e)
        {
            itemp = double.Parse(textbox1.Text);
            totalcost = itemp + totalcost;

            label2.Text = $"Added amount ${itemp}";
            textbox1.Text = String.Empty;
        }

        private void bt2_Click(object sender, RoutedEventArgs e)
        {
            var answer = "";
            answer = $"The total cost of the shopping including GST is ${gst(totalcost)}";

            label3.Text = answer;
            textbox1.Text = String.Empty;
        }



        static string q1 = "Please enter the price of the item: ";
        static double totalcost = 0;
        static double itemp = 0;

        static string gst(double totlcost)
        {

            var output = "";
            totlcost = totlcost * 1.15;
            totlcost = System.Math.Round(totlcost, 2);
            output = Convert.ToString(totlcost);

            return output;
        }
    }
}