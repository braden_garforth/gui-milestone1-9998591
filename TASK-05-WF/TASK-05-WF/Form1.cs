﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TASK_05_WF
{
    public partial class Form1 : Form
    {
        static int points = 0;
        static int random = 0;
        static int turns = 0;

        static string opt1 = "Guess a number between 1 and 5 and see if you can \nmatch the random number given by the program";
        static string opt2 = "you get 5 turns, see how many you can guess correctly";
        static string q1 = "Enter your guess";

        public Form1()
        {
            InitializeComponent();
            label1.Text = opt1;
            label2.Text = opt2;
            label3.Text = q1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (turns < 4)
            {
                label4.Text = rightorwrong(int.Parse(textBox1.Text));
                label5.Text = $"you have a score of {points} well done";
            }
            else
            {
                button1.Visible = false;
                label4.Text = rightorwrong(int.Parse(textBox1.Text));
                label5.Text = $"you got a score of {points} !";         
            
            }
            
        }
            static string rightorwrong(int check)
        {
                var answer = "";
                var rand = randomnum(random);

                if (randomnum(random) == check)
                {
                    points = points + 1;
                    answer = $"you guessed {check} and the correct answer was {randomnum(random)}.\n Press enter to have your next turn.";
                }
                else
                {
                    answer = $"you guessed {check} and the correct answer was  {randomnum(random)}.\n Press enter to have your next turn.";
                }
                turns++;
                return answer;
            }

        static int randomnum(int random)
        {
            Random rand = new Random();
            int answer = rand.Next(1, 6);

            return answer;
        }
    }
    }

