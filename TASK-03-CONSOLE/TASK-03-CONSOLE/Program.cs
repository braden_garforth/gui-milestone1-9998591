﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TASK_03_CONSOLE
{
    class Program
    {
        static string q1 = "please select what options of things you would like to view";
        static string q2 = "[0]meats [1]vegatables [2]fruits [3]EXIT";

        static void Main(string[] args)
        {
      

            while (true)
            {

                Console.Clear();
                Console.WriteLine(q1);
                Console.WriteLine(q2);
                var choice1 = int.Parse(Console.ReadLine());
                if (choice1 < 3)
                {
                    Console.WriteLine($"{options(choice1)}");
                    Console.WriteLine($"Press <ENTER> when you are done");
                    Console.ReadKey();
                }
                else
                {
                    break;
                }
            }
        }

        static string options(int choice)
        {
            var output = "";

            switch (choice)
            {
                case 0:
                    Console.Clear();
                    output = "steak, lamb , mince , chicken ";
                    break;
                case 1:
                    Console.Clear();
                    output = "broccoli, peas, beans";
                    break;
                case 2:
                    Console.Clear();
                    output = "tomato, apple, banana, ";
                    break;
                case 3:
                    break;
            }
            return output;

        }
    }
}
