﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace TASK_03_UWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {

        static string q1 = "please select what list of things you would like to look at.";
        public MainPage()
        {
            this.InitializeComponent();
            question.Text = q1;
        }
        private void choice_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            answer.Text = list(combobox1.SelectedIndex);
        }

        static string list(int choice)
        {
            var output = "";

            switch (choice)
            {
                case 0:

                    output = "steak, lamb , mince , chicken ";
                    break;
                case 1:

                    output = "broccoli, peas, beans";
                    break;
                case 2:

                    output = "tomato, apple, banana, ";
                    break;
                default:
                    break;
            }
            return output;

        }
    }
}