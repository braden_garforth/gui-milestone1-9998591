﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TASK_01_WF
{
    public partial class Form1 : Form
    {
        public Form1()
        { 
         InitializeComponent();

            label1.Text = q1;
            label2.Text = q2;

        }
        static string q1 = "Enter the amount of distance you want to convert";
        static string q2 = "Do you want to convert it to [0]miles or [1]kilometers?";

        static string choices(string number, int choice)
    {
        var answer = "";

        switch (choice)
        {

            case 0:
                answer = $"{number} Kilometers is equal to {kilometers2miles(double.Parse(number))} Miles";
                break;

            case 1:
                answer = $"{number} Miles is equal to {miles2kilometers(double.Parse(number))} Kilometers";
                break;

            default:
                answer = $"Incorrect value given, Next time use 0 for Kilometers and 1 for Miles";
                break;
        }

        return answer;
    }

    static double miles2kilometers(double input)
    {
        const double converter = 1.609344;
        var mile = Convert.ToDouble(input);
        var result = mile * converter;

        result = System.Math.Round(result, 2);
        return result;
    }

    static double kilometers2miles(double input)
    {
        const double converter = 0.621371192;
        var km = Convert.ToDouble(input);
        var result = km * converter;

        result = System.Math.Round(result, 2);
        return result;
    }

    private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
    {
        label3.Text = choices(textBox1.Text, comboBox1.SelectedIndex);
    }
}
}