﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace TASK_04_UWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        static string q1 = "Hello there welcome to the fruit and vege finder!";
        static string q2 = "Tell me what you are looking for and ill see if we have it!";
        public MainPage()
        {
            this.InitializeComponent();
            label1.Text = q1;
            label2.Text = q2;
        }
        private void check_Click(object sender, RoutedEventArgs e)
        {
            answer.Text = instock(input.Text);
        }
        static string instock(string check)
        {
            var output = "";
            var dictionary = new Dictionary<string, string>();

            dictionary.Add("mango", "fruit");
            dictionary.Add("mangos", "fruit");
            dictionary.Add("mangoes", "fruit");
            dictionary.Add("kiwifruit", "fruit");
            dictionary.Add("kiwifruits", "fruit");
            dictionary.Add("banana", "fruit");
            dictionary.Add("Melon", "fruit");
            dictionary.Add("Melons", "fruit");
            dictionary.Add("pear", "fruit");
            dictionary.Add("pears", "fruit");
            dictionary.Add("Apple", "fruit");
            dictionary.Add("Apples", "fruit");
            dictionary.Add("Apricot", "fruit");
            dictionary.Add("cucumber", "vegetable");
            dictionary.Add("cucumbers", "vegetable");
            dictionary.Add("lettuce", "vegetable");
            dictionary.Add("leeks", "vegetable");
            dictionary.Add("celery", "vegetable");
            dictionary.Add("carrot", "vegetable");
            dictionary.Add("carrots", "vegetable");
            dictionary.Add("potato", "vegetable");
            dictionary.Add("potatos", "vegetable");
            dictionary.Add("potatoes", "vegetable");
            dictionary.Add("cabbage", "vegetable");
            dictionary.Add("cabbages", "vegetable");
            dictionary.Add("Asparagus", "vegetable");
            dictionary.Add("Blackberry", "vegtable");
            dictionary.Add("Blackberries", "vegtable");
            dictionary.Add("Eggplant", "vegetable");
            dictionary.Add("Eggplants", "vegetable");
            if (dictionary.ContainsKey(check))
            {
                output = $"Yes we have {check} in stock";
            }

            else
            {
                output = $"sorry we dont have {check} in stock";
            }

            return output;
        }
    }
}
