﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TASK_04_WF
{
    public partial class Form1 : Form
    {
        static string q1 = "Hello there welcome to the fruit and vege finder!";
        static string q2 = "Tell me what you are looking for and ill see if we have it!";
        public Form1()
        {
            InitializeComponent();
            label1.Text = q1;
            label2.Text = q2;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label3.Text = instock(textBox1.Text);
        }
        static string instock(string check)
        {
            var output = "";
            var dictionary = new Dictionary<string, string>();

            dictionary.Add("mango", "fruit");
            dictionary.Add("mangos", "fruit");
            dictionary.Add("mangoes", "fruit");
            dictionary.Add("kiwifruit", "fruit");
            dictionary.Add("kiwifruits", "fruit");
            dictionary.Add("banana", "fruit");
            dictionary.Add("Melon", "fruit");
            dictionary.Add("Melons", "fruit");
            dictionary.Add("pear", "fruit");
            dictionary.Add("pears", "fruit");
            dictionary.Add("Apple", "fruit");
            dictionary.Add("Apples", "fruit");
            dictionary.Add("Apricot", "fruit");
            dictionary.Add("cucumber", "vegetable");
            dictionary.Add("cucumbers", "vegetable");
            dictionary.Add("lettuce", "vegetable");
            dictionary.Add("leeks", "vegetable");
            dictionary.Add("celery", "vegetable");
            dictionary.Add("carrot", "vegetable");
            dictionary.Add("carrots", "vegetable");
            dictionary.Add("potato", "vegetable");
            dictionary.Add("potatos", "vegetable");
            dictionary.Add("potatoes", "vegetable");
            dictionary.Add("cabbage", "vegetable");
            dictionary.Add("cabbages", "vegetable");
            dictionary.Add("Asparagus", "vegetable");
            dictionary.Add("Blackberry", "vegtable");
            dictionary.Add("Blackberries", "vegtable");
            dictionary.Add("Eggplant", "vegetable");
            dictionary.Add("Eggplants", "vegetable");
            if (dictionary.ContainsKey(check))
            {
                output = $"Yes we have {check} in stock";
            }

            else
            {
                output = $"sorry we dont have {check} in stock";
            }

            return output;
        }
    }
}

