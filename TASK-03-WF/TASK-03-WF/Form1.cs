﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TASK_03_WF
{
    public partial class Form1 : Form
    {
        static string q1 = "Please select what list youd like to view";

        public Form1()
        {
            InitializeComponent();
            label1.Text = q1;
        }


        static string list(int choice)
        {
            var output = "";

            switch (choice)
            {
                case 0:
                   
                    output = "steak, lamb , mince , chicken ";
                    break;
                case 1:
                   
                    output = "broccoli, peas, beans";
                    break;
                case 2:
                  
                    output = "tomato, apple, banana, ";
                    break;
                default:
                    break;
            }
            return output;

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            label2.Text = list(comboBox1.SelectedIndex);
        }
    }
}
