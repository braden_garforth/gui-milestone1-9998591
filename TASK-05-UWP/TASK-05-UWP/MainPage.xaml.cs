﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace TASK_05_UWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        static int points = 0;
        static int random = 0;
        static int turns = 0;

        static string opt1 = "Guess a number between 1 and 5 and see if you can \nmatch the random number given by the program";
        static string opt2 = "you get 5 turns, see how many you can guess correctly";
        static string q1 = "Enter your guess";
        public MainPage()
        {
            this.InitializeComponent();
            label1.Text = opt1;
            label2.Text = opt2;
            label3.Text = q1;
        }
        private void button1_Click(object sender, RoutedEventArgs e)
        {
            {
                if (turns < 4)
                {
                    label4.Text = rightorwrong(int.Parse(textBox1.Text));
                    label5.Text = $"you have a score of {points}!";
                }
                else
                {
                    button1.Visibility = Visibility.Collapsed;
                    label4.Text = rightorwrong(int.Parse(textBox1.Text));
                    label5.Text = $"you got a score of {points} !";

                }

            }
        }
            static string rightorwrong(int check)
        {
            var answer = "";
            var rand = randomnum(random);

            if (randomnum(random) == check)
            {
                points = points + 1;
                answer = $"you guessed {check} and the correct answer was {randomnum(random)}. \nPress enter to have your next turn.";
            }
            else
            {
                answer = $"you guessed {check} and the correct answer was  {randomnum(random)}. \nPress enter to have your next turn.";
            }
            turns++;
            return answer;
        }

        static int randomnum(int random)
        {
            Random rand = new Random();
            int answer = rand.Next(1, 6);

            return answer;
        }

    }
}